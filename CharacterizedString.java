import java.util.Scanner;

public class CharacterizedString {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String nama;

        // Input Name
        System.out.print("Ketik Kata/Kalimat, lalu tekan enter: ");
        nama = input.nextLine();

        // If Statement
        if (nama.equals("") || nama.equals(null)) {
            System.out.println("Harap Mengisi Kata/Kalimat");
        } else {

            System.out.println("=================================");
            System.out.println("String Penuh: " + nama);
            System.out.println("=================================");
            System.out.println("Process memecahkan Kata/Kalimat menjadi karakter....");
            System.out.println("...........");
            System.out.println("Done.");
            System.out.println("=================================");

            for (int i = 1; i <= nama.length(); i++) {
                System.out.println("Karakter [" + i + "]: " + nama.charAt(i - 1));

            }

        }

    }
}